"""
Coffee Shop program improvements. In addition to the previously created program we will add
the following parameters: tax - for each payment in percentage we charge a tax bid, a client can order
the same coffee multiple times. Display the result using string format and methods learned
in the lesson
"""
if __name__ == '__main__':
    welcome_text = "Welcome to Mars Coffee \nWe have three type of coffees"
    print(welcome_text)
    americano = "Americano"
    cappuccino = "Cappuccino"
    latte = "Latte"

    menu = f"{americano} \n{cappuccino} \n{latte}"
    print(menu)

    coffee_type = input("Please enter a coffee you want: ")
    coffee_number = input("How many coffee you want? Please enter the amount greater than 0: ")
    print("Nice, I am starting making your coffee")

    price = 12
    tax = 15

    coffee_price = price * int(coffee_number)
    tax_price = coffee_price * (tax/100)
    total_price = coffee_price + tax_price
    coffee_choice = (coffee_type.capitalize() + ": " + str(price) + "$\n") * int(coffee_number)

    recipe = f"Your order: \n{coffee_choice}\n" \
             f"---------------------\n" \
             f"Coffee price: {coffee_price}$\n" \
             f"Tax: {tax}%\n" \
             f"Total: {total_price:.2f}$"
    print("Your order is ready\n")
    print(recipe)


