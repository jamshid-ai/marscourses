text_a = "Hello"
text_b = "Mars"

# using +
text_result = text_a + " " + text_b
print(text_result)

# using *
text_result = text_a + " " + text_b * 3
# Let students guess the output
print(text_result)

# Define the length of the text
text_lng = """String concatenation is a common operation 
in many programming 
languages where you combine two or more strings into one."""

# Here should be error, explain and make sure students get it
print("The length of the text: " + len(text_lng))
