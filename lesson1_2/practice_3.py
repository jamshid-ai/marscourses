c = None
print("Result of a+b still not defined", c)
a = 1
b = 2

c = a + b
print("Now we know the result of a+b", c)

# Explain the error
c = None + a
print(c)