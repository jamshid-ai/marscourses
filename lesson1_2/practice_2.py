
a_ = "1"
b2b = "2"
_ = 'e'
Abc = 1
aBc = 2
# Explain the cause of the error
4c = "a+b=3"
_ = "Hello "

cat = "Say Meow"
dog = "Say Wof"

# Undefined variables
african_animals = giraffe

print("I hear:", cat, dog, african_animals)

