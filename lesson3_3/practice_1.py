a = -1
b = 2
if a > b:
    print(f"{a} greater than {b}")
if a == b:
    print(f"{a} equal to {b}")
if a < b:
    print(f"{a} less than {b}")

# we can compare strings
animal = "cat"
if animal == 'dog':
    print("Dog says wouf")
if animal == 'cat':
    print("Cat says meow")

# Ask students why it won't print out, since comparing string it compares each character for full similarity
if animal == 'Cat':
    print('Cat says meow')

# we can compare expression within if operator, let students guess the results
a_f = 0.5
b_f = 2.0
if a_f + b_f > 2:
    print(f"the sum of {a_f} and {b_f} greater than 2")
if a_f * b_f > 0:
    print(f"multiplying {a_f} to {b_f} greater than 0")
if b_f / a_f > 4:
    print(f"division {b_f} by {a_f} greater than 2")

# what happen if compare equal numbers?
if 4 < 4:
    print("Four is less than 4")










