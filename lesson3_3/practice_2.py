# Using not equal and <= or >=
if 4 != 5:
    print("they are not equal")

x = 4
if x/4 != 0:
    print("It is not equal to 0")

x = 5
y = 16
if y % x != 0:
    print(f"{y} is not numerical divisional by {x}")

a = 6
b = 7

if a - b <= 0:
    print("It's 0 or negative value")
if a - b + 1 >= 0:
    print("It's 0 or positive value")
