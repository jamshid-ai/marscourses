
"""
Phone Book 2. Use a phone book to store clients' data: phone, name, city, address. Program allows a user to find client's
info by his/her phone number. Use a user input to enter a phone number. If there is no information, print a
text: ""We cannot find your requested phone number, try another phone number"". Ask for enter numbers unless EXIT
phrase is entered.
Hint: use dict key as a phone number. Value is another dict object to store user info.
"""

PHONE_BOOK = {
    "90981212": {"name": "Bob", "city": "Washington", "address": "Dublin str, 123A, US"},
    "90981213": {"name": "Alice", "city": "Toronto", "address": "Magic str, 12, CA"},
    "90981214": {"name": "Pit", "city": "Sydney", "address": "Walker str, 13B, AU"},
    "90981215": {"name": "Fred", "city": "Tashkent", "address": "Bobur str, 23A, UZ"},
}


def check_for_phone(phone):
    return PHONE_BOOK.get(phone)


if __name__ == '__main__':
    selects = []
    phone = input("Enter a phone: ")

    while phone.upper() != "EXIT":
        info = check_for_phone(phone)
        print(info)

        phone = input("Enter a phone: ")

    print("Thank you for using our service!")
