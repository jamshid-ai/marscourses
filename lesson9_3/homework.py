"""
Phone Book. Use a phone book to store clients' data: phone, name. Program allows a user to find client's
name by his/her phone number. Use a user input to enter a phone number. If there is no information, print a
text: "We cannot find your requested phone number, try another phone number". Ask for enter numbers unless EXIT
phrase is entered. Hint: use dict key as a phone number. Value is a name.
"""

PHONE_BOOK = {
    "9933": "Peter",
    "9332": "Sean",
    "9683": "Tom",
    "0916": "Adam",
    "1935": "James",
}


def finder(phone):
    result = None
    if phone in PHONE_BOOK:
        name = PHONE_BOOK[phone]
        result = f"This phone owner's name is: {name}"
        print(result)

    return result


if __name__ == '__main__':
    while True:
        phone_number = input("Please enter a phone number or EXIT")
        if phone_number == 'EXIT':
            print("Thank you for using our service. Bye!")
            quit(1)
        if not finder(phone_number):
            print("We cannot find your requested phone number, try another phone number")

