# A Rectangle is a four sided-polygon, having all the internal angles equal to 90 degrees.
import math

print("Perimeter of a Rectangle")
length = input("Enter the length:")
width = input("Enter the width:")

perimeter = 2 * (int(length) + int(width))
print("Perimeter is equal to: ", perimeter)

print("Area of Rectangle")
length = input("Enter the length:")
width = input("Enter the width:")

area = int(length) + int(width)
print("Area is equal to: ", area)

# Python "math" module and its methods
pi = math.pi
square_t = math.sqrt(4)
print(square_t)

# Explain about ceil
result = math.ceil(2*2.6)
print(result)
