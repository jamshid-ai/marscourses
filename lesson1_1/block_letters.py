# Homework
"""
Print your initials with each letter fit to 7x5(7 lines and 5 columns) block
Example Sultonov Latif initials will be S L, and it will be printed

    12345  12345
1    SSS   L
2   S   S  L
3   S      L
4    SSS   L
5       S  L
6   S   S  L
7    SSS   LLLLL

"""

print("  SSS   L     ")
print(" S   S  L     ")
print(" S      L     ")
print("  SSS   L     ")
print("     S  L     ")
print(" S   S  L     ")
print("  SSS   LLLLL ")