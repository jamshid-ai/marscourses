# if operator accepts only boolean expressions and process inside block only if expression accepts True
if True:
    print("You right")

# explain why it never prints out the text
if False:
    print("You wrong")

bool_value = False
if bool_value:
    print("Looser")
else:
    print("Winner")

# List all values that return always False
val_1 = ""
val_2 = None
val_3 = 0
if val_1:
    print("Empty string is always False, so you never see this message")

if val_2:
    print("None value is always False, so yes you will never reach this block of code")

if val_3:
    print("0 number represents False value, yes this message will never be printed out")


a_int = 1
b_int = 2

negative = False
zero = False

if negative:
    result = a_int-b_int
elif zero:
    result = a_int * b_int * 0
else:
    result = a_int + b_int

print(result)

# Let students to find out
if True
    print("Yes")
If False
    print("No")
elif:
    print("Yes")
else:
print("default")