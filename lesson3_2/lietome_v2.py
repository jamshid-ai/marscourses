
"""
Lie To Me. Create a program with five input questions, answer can be True or False,
as results it should sum all your answers and print out some score you answered right.
Hint: 0 and 1 can be converted as False and True
"""

print("Hello stranger, you are standing here to get all my gold treasures")
print("But you must pass my Quiz, If you collect the best scores the treasures yours, get ready!")
print("I accept only 0 or 1: 0 is NO, 1 is YES")

score = 0
right_answer = 0
answer_1 = input("The capital of USA is New York, it's YES or NO?: ")
if bool(answer_1):
    score = score - 1
else:
    right_answer = right_answer + 1
    score = score + 5

answer_2 = input("One kilogram water is heavier than one kilogram stone, it's YES or NO?: ")
if bool(answer_2):
    score = score - 1
else:
    right_answer = right_answer + 1
    score = score + 10

answer_3 = input("The highest building in the world is Burj Khalifa, it's YES or NO?: ")
if bool(answer_3):
    right_answer = right_answer + 1
    score = score + 12
else:
    score = score - 1

answer_4 = input("Ostriches bury their head in the sand due to fear, it's YES or NO?: ")
if bool(answer_4):
    score = score - 1
else:
    right_answer = right_answer + 1
    score = score + 15

answer_5 = input("Camels carry water in their humps, it's YES or NO?: ")
if bool(answer_5):
    score = score - 1
else:
    right_answer = right_answer + 1
    score = score + 21

print(f"Your score is: {score}")
print(f"Your answered correctly to {right_answer} questions")
