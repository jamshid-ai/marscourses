"""
EcoBazar 2. There are five items in the store: Apples, Oranges, Tomatoes, Potatoes, Onions.
Each item has a price per  kilogram. Create user input asking to choose an item and amount of kilos. Use list to
store input values. Store prices in the list. Print out a total value of price per chosen item.
Hints: items also stored in the list, user inputs the name of item.
"""

ITEMS = ["Apples", "Oranges", "Tomatoes", "Potatoes", "Onions"]
PRICES = [20, 30, 15, 10, 5]

SELECTED_ITEMS = []
ITEMS_PRICE = []


def item_prices(itm, kilos):
    idx = ITEMS.index(itm)
    price = PRICES[idx]
    total_price = kilos * price
    return total_price


if __name__ == '__main__':
    print(f"Welcome to EcoBazar2, we have")
    print("Apples")
    print("Oranges")
    print("Tomatoes")
    print("Potatoes")
    print("Onions")

    for i in range(2):
        item = input("Please select one of the them.")
        if item not in ITEMS:
            item = input(f"We don't have \"{item}\" in our store, choose other items")

        if item not in ITEMS:
            print(f"We don't have \"{item}\" in our store again, Bye!")
            exit(1)

        SELECTED_ITEMS.append(item)
        kilo = input(f"You chose {item}, how many kilos you want? Enter numeric data!")

        total_price = item_prices(item, float(kilo))
        ITEMS_PRICE.append(total_price)
        print("Select your second item")

    print(f"You chose {SELECTED_ITEMS}")
    total_sum = sum(ITEMS_PRICE)
    print(f"You pay: {total_sum}$")
